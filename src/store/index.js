import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { reducer as productReducer, MODULE_NAME as productsModuleName } from './productList';
import { reducer as popupReducer, MODULE_NAME as popupModuleName } from './popup';

const rootReducer = combineReducers({
    [productsModuleName]: productReducer,
    [popupModuleName]: popupReducer
});

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));